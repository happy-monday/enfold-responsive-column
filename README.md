# Enfold Responsive Columns
This plugin for WordPress relies on the Enfold theme being installed and adds a new element, very similar to the grid row element, except it is restricted to two columns. You can then set the preference whether you'd like the last column to jump to the top on mobile views. This alows you to have a format like:
```
| ------------ |
|   <img>      |
| ------------ |
|   col 1 .    |
| ------------ |
|   <img>      |
| ------------ |
|   col 2 .    |
| ------------ |
```
`@media screen and (min-width: 768px)`
```
| ------------ | ------------ |
|  col 1 .     |   <img>      |
| ------------ | ------------ |
|  <img>       |   col 2 .    |
| ------------ | ------------ |
```

## Installation

1. First install [this plugin](https://github.com/afragen/github-updater/wiki/Installation) using the "Upload" method detailed there.
2. Activate the plugin and navigate to the settings page
3. Click "Install Plugin"
4. In the "Plugin URI" enter: https://bitbucket.org/digital-operative/enfold-responsive-column
5. Set the "Remote Repository Host" to "Bitbucket"
6. Install Plugin

Doing this will allow this plugin to receive updates whenever we release them.