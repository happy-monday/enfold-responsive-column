<?php

/**
 * Grid Row
 * 
 * Shortcode which adds multiple Grid Rows below each other to create advanced grid layouts. Cells can be styled individually
 */

// Don't load directly
if (!defined('ABSPATH')) {
  die('-1');
}

if (!class_exists('avia_sc_responsive_columns')) {

  class avia_sc_responsive_columns extends aviaShortcodeTemplate
  {

    static $count = 0;


    /**
     * Create the config array for the shortcode grid row
     */
    function shortcode_insert_button()
    {
      $this->config['type']        =  'layout';
      $this->config['self_closing']    =  'no';
      $this->config['contains_text']    =  'no';
      $this->config['layout_children']  =  array(
        'av_cell_one_half',
      );


      $this->config['name']    = __('Responsive Columns', 'avia_framework');
      $this->config['icon']    = AviaBuilder::$path['imagesURL'] . "sc-layout_row.png";
      $this->config['tab']    = __('Layout Elements', 'avia_framework');
      $this->config['order']    = 15;
      $this->config['shortcode']   = 'av_layout_responsive_columns';
      $this->config['html_renderer']   = false;
      $this->config['tinyMCE']   = array('disable' => "true");
      $this->config['tooltip']   = __('Add a two-column row that can be re-ordered on mobile. Cells can be styled individually', 'avia_framework');
      $this->config['drag-level'] = 1;
      $this->config['drop-level'] = 100;
      $this->config['disabling_allowed'] = false;
    }

    function admin_assets()
    {
      wp_enqueue_style('avia-admin-module-responsivecol', Enfold_Responsive_Column::get_url() . 'templatebuilder/responsive_columns/admin.css', array(), Enfold_Responsive_Column::VERSION);
    }

    function extra_assets()
    {
      //load css
      wp_enqueue_style('avia-module-responsivecol', Enfold_Responsive_Column::get_url() . 'templatebuilder/responsive_columns/responsive_columns.css', array('avia-layout'), Enfold_Responsive_Column::VERSION);
    }


    /**
     * Editor Element - this function defines the visual appearance of an element on the AviaBuilder Canvas
     * Most common usage is to define some markup in the $params['innerHtml'] which is then inserted into the drag and drop container
     * Less often used: $params['data'] to add data attributes, $params['class'] to modify the className
     *
     *
     * @param array $params this array holds the default values for $content and $args.
     * @return $params the return array usually holds an innerHtml key that holds item specific markup.
     */

    function editor_element($params)
    {

      /*
				$params['content'] = trim($params['content']);
				if(empty($params['content'])) $params['content'] = "[av_cell_one_half first][/av_cell_one_half] [av_cell_one_half][/av_cell_one_half]";
*/


      extract($params);

      $name = $this->config['shortcode'];
      $data['shortcodehandler']   = $this->config['shortcode'];
      $data['modal_title']     = $this->config['name'];
      $data['modal_ajax_hook']   = $this->config['shortcode'];
      $data['dragdrop-level']   = $this->config['drag-level'];
      $data['allowed-shortcodes']  = $this->config['shortcode'];

      if (!empty($this->config['modal_on_load'])) {
        $data['modal_on_load']   = $this->config['modal_on_load'];
      }

      $dataString  = AviaHelper::create_data_string($data);


      if ($content) {
        $final_content = $this->builder->do_shortcode_backend($content);
        $text_area = ShortcodeHelper::create_shortcode_by_array($name, $content, $args);
      } else {
        $cell = new avia_sc_cell_one_half($this->builder);
        $params = array('content' => "", 'args' => array(), 'data' => '');
        $final_content  = "";
        $final_content .= $cell->editor_element($params);
        $final_content .= $cell->editor_element($params);
        $text_area = ShortcodeHelper::create_shortcode_by_array($name, '[av_cell_one_half][/av_cell_one_half] [av_cell_one_half][/av_cell_one_half]', $args);
      }

      $title_id = !empty($args['id']) ? ": " . ucfirst($args['id']) : "";
      $hidden_el_active = !empty($args['av_element_hidden_in_editor']) ? "av-layout-element-closed" : "";



      $output  = "<div class='avia_responsive_columns avia_layout_row {$hidden_el_active} avia_layout_section avia_pop_class avia-no-visual-updates " . $name . " av_drag' " . $dataString . ">";

      $output .= "    <div class='avia_sorthandle menu-item-handle'>";
      $output .= "        <span class='avia-element-title'>" . $this->config['name'] . "<span class='avia-element-title-id'>" . $title_id . "</span></span>";
      $output .= "        <a class='avia-delete'  href='#delete' title='" . __('Delete Row', 'avia_framework') . "'>x</a>";
      $output .= "        <a class='avia-toggle-visibility'  href='#toggle' title='" . __('Show/Hide Section', 'avia_framework') . "'></a>";

      if (!empty($this->config['popup_editor'])) {
        $output .= "    <a class='avia-edit-element'  href='#edit-element' title='" . __('Edit Row', 'avia_framework') . "'>" . __('edit', 'avia_framework') . "</a>";
      }
      $output .= "<a class='avia-save-element'  href='#save-element' title='" . __('Save Element as Template', 'avia_framework') . "'>+</a>";
      $output .= "        <a class='avia-clone'  href='#clone' title='" . __('Clone Row', 'avia_framework') . "' >" . __('Clone Row', 'avia_framework') . "</a></div>";
      $output .= "    <div class='avia_inner_shortcode avia_connect_sort av_drop' data-dragdrop-level='" . $this->config['drop-level'] . "'>";
      $output .= "<textarea data-name='text-shortcode' cols='20' rows='4'>" . $text_area . "</textarea>";
      $output .= $final_content;

      $output .= "</div>";

      $output .= "<a class='avia-layout-element-hidden' href='#'>" . __('Grid Row content hidden. Click here to show it', 'avia_framework') . "</a>";

      $output .= "</div>";

      return $output;
    }

    /**
     * Popup Elements
     *
     * If this function is defined in a child class the element automatically gets an edit button, that, when pressed
     * opens a modal window that allows to edit the element properties
     *
     * @return void
     */
    function popup_elements()
    {
      global $avia_config;


      $this->elements = array(
        array(
          "type"   => "tab_container", 'nodescription' => true
        ),

        array(
          "type"   => "tab",
          "name"  => __("Content", 'avia_framework'),
          'nodescription' => true
        ),
        array(
          "name" => __("Grid Width", 'avai_framework'),
          "id" => "grid_width",
          "desc" => __("Set the grid to either be full-width or the content width", 'avia_framework'),
          "type" => "select",
          "std" => "fullwidth",
          "subtype" => array(
            __("Full-width", 'avia_framework') => 'fullwidth',
            __("Content-width", 'avia_framework') => 'contentwidth',
          )
        ),
        array(
          "name"   => __("Grid Borders", 'avia_framework'),
          "id"   => "border",
          "desc"  => __("Choose if your layout grid should display any border", 'avia_framework'),
          "type"   => "select",
          "std"   => "",
          "subtype" => array(
            __('No Borders', 'avia_framework') => '',
            __('Borders on top and bottom', 'avia_framework') => 'av-border-top-bottom',
            __('Borders between cells', 'avia_framework') => 'av-border-cells',
            __('Borders on top and bottom and between cells', 'avia_framework') => 'av-border-top-bottom av-border-cells',
          )
        ),


        array(
          "name"   => __("Custom minimum height", 'avia_framework'),
          "desc"   => __("Do you want to use a custom or predefined minimum height?", 'avia_framework'),
          "id"   => "min_height_percent",
          "type"   => "select",
          "std"   => "",
          "subtype" => array(
            __('At least 100&percnt; of Browser Window height', 'avia_framework') => '100',
            __('At least 75&percnt; of Browser Window height', 'avia_framework')  => '75',
            __('At least 50&percnt; of Browser Window height', 'avia_framework')  => '50',
            __('At least 25&percnt; of Browser Window height', 'avia_framework')  => '25',
            __('Custom height in pixel', 'avia_framework')  => '',
          )
        ),


        array(
          "name"   => __("Minimum height", 'avia_framework'),
          "desc"   => __("Set the minimum height of all the cells in pixel. eg:400px", 'avia_framework'),
          "id"   => "min_height",
          "required" => array('min_height_percent', 'equals', ''),
          "type"   => "input",
          "std"   => "0",
        ),
        array(
          "name"   => __("Section Padding", 'avia_framework'),
          "id"   => "padding",
          "desc"  => __("Define the sections top and bottom padding", 'avia_framework'),
          "type"   => "select",
          "std"   => "default",
          "subtype" => array(
            __('No Padding', 'avia_framework')  => 'no-padding',
            __('Small Padding', 'avia_framework')  => 'small',
            __('Default Padding', 'avia_framework')  => 'default',
            __('Large Padding', 'avia_framework')  => 'large',
            __('Huge Padding', 'avia_framework')  => 'huge',
          )
        ),


        array(
          "name"   => __("Section Colors", 'avia_framework'),
          "id"   => "color",
          "desc"  => __("The section will use the color scheme you select. Color schemes are defined on your styling page", 'avia_framework') .
            '<br/><a target="_blank" href="' . admin_url('admin.php?page=avia#goto_styling') . '">' . __("(Show Styling Page)", 'avia_framework') . "</a>",
          "type"   => "select",
          "std"   => "main_color",
          "subtype" =>  array_flip($avia_config['color_sets'])
        ),


        array(
          "name"   => __("Mobile Behaviour", 'avia_framework'),
          "id"   => "mobile",
          "desc"  => __("Choose how the cells inside the grid should behave on mobile devices and small screens", 'avia_framework'),
          "type"   => "select",
          "std"   => "av-erc-stack-cells",
          "subtype" => array(
            __('Default: Stack each cell in the order they appear', 'avia_framework') => 'av-erc-stack-cells',
            __('Bring the last cell to the top', 'avia_framework') => 'av-erc-flip-cells',
          )
        ),

        array(
          "name"   => __("For Developers: Section ID", 'avia_framework'),
          "desc"   => __("Apply a custom ID Attribute to the section, so you can apply a unique style via CSS. This option is also helpful if you want to use anchor links to scroll to a sections when a link is clicked", 'avia_framework') . "<br/><br/>" .
            __("Use with caution and make sure to only use allowed characters. No special characters can be used.", 'avia_framework'),
          "id"   => "id",
          "type"   => "input",
          "std" => ""
        ),

        array(
          "id"   => "av_element_hidden_in_editor",
          "type"   => "hidden",
          "std" => "0"
        ),

        array(
          "type"   => "close_div",
          'nodescription' => true
        ),
        array(
          "type"   => "tab",
          "name"  => __("Section Background", 'avia_framework'),
          'nodescription' => true
        ),

        array(
          "name"   => __("Background", 'avia_framework'),
          "desc"   => __("Select the type of background for the column.", 'avia_framework'),
          "id"   => "background",
          "type"   => "select",
          "std"   => "bg_color",
          "subtype" => array(
            __('Background Color', 'avia_framework') => 'bg_color',
          )
        ),

        array(
          "name"   => __("Custom Background Color", 'avia_framework'),
          "desc"   => __("Select a custom background color for this cell here. Leave empty for default color", 'avia_framework'),
          "id"   => "custom_bg",
          "type"   => "colorpicker",
          "required" => array('background', 'equals', 'bg_color'),
          "rgba"   => true,
          "std"   => "",
        ),

        array(
          "name"   => __("Custom Background Image", 'avia_framework'),
          "desc"   => __("Either upload a new, or choose an existing image from your media library. Leave empty if you want to use the background image of the color scheme defined above", 'avia_framework'),
          "id"   => "src",
          "type"   => "image",
          "title" => __("Insert Image", 'avia_framework'),
          "button" => __("Insert", 'avia_framework'),
          "std"   => ""
        ),

        array(
          "name"   => __("Background Attachment", 'avia_framework'),
          "desc"   => __("Background can either scroll with the page, be fixed or scroll with a parallax motion", 'avia_framework'),
          "id"   => "attach",
          "type"   => "select",
          "std"   => "scroll",
          "required" => array('src', 'not', ''),
          "subtype" => array(
            __('Scroll', 'avia_framework') => 'scroll',
            __('Fixed', 'avia_framework') => 'fixed',
            __('Parallax', 'avia_framework') => 'parallax'

          )
        ),

        array(
          "name"   => __("Background Image Position", 'avia_framework'),
          "id"   => "position",
          "type"   => "select",
          "std"   => "top left",
          "required" => array('src', 'not', ''),
          "subtype" => array(
            __('Top Left', 'avia_framework')       => 'top left',
            __('Top Center', 'avia_framework')     => 'top center',
            __('Top Right', 'avia_framework')      => 'top right',
            __('Bottom Left', 'avia_framework')    => 'bottom left',
            __('Bottom Center', 'avia_framework')  => 'bottom center',
            __('Bottom Right', 'avia_framework')   => 'bottom right',
            __('Center Left', 'avia_framework')    => 'center left',
            __('Center Center', 'avia_framework')  => 'center center',
            __('Center Right', 'avia_framework')   => 'center right'
          )
        ),

        array(
          "name"   => __("Background Repeat", 'avia_framework'),
          "id"   => "repeat",
          "type"   => "select",
          "std"   => "no-repeat",
          "required" => array('src', 'not', ''),
          "subtype" => array(
            __('No Repeat', 'avia_framework')          => 'no-repeat',
            __('Repeat', 'avia_framework')             => 'repeat',
            __('Tile Horizontally', 'avia_framework')  => 'repeat-x',
            __('Tile Vertically', 'avia_framework')    => 'repeat-y',
            __('Stretch to fit (stretches image to cover the element)', 'avia_framework')     => 'stretch',
            __('Scale to fit (scales image so the whole image is always visible)', 'avia_framework')     => 'contain'
          )
        ),

        array(
          "type"   => "close_div",
          'nodescription' => true
        ),

        array(
          "type"   => "tab",
          "name"  => __("Screen Options", 'avia_framework'),
          'nodescription' => true
        ),

        array(
          "name"   => __("Mobile Breaking Point", 'avia_framework'),
          "desc"   => __("Set the screen width when cells in this row should switch to full width", 'avia_framework'),
          "type"   => "heading",
          "description_class" => "av-builder-note av-neutral",
        ),


        array(
          "name"   => __("Fullwidth Break Point", 'avia_framework'),
          "desc"   => __("The cells in this row will switch to fullwidth at this screen width ", 'avia_framework'),
          "id"   => "mobile_breaking",
          "type"   => "select",
          "std"   => "",
          "subtype" => array(
            __('On mobile devices (at a screen width of 767px or lower)', 'avia_framework') => '',
            __('On tablets (at a screen width of 989px or lower)',  'avia_framework') => 'av-break-at-tablet',
          )
        ),



        array(
          "name"   => __("Element Visibility", 'avia_framework'),
          "desc"   => __("Set the visibility for this element, based on the device screensize.", 'avia_framework'),
          "type"   => "heading",
          "description_class" => "av-builder-note av-neutral",
        ),

        array(
          "desc"   => __("Hide on large screens (wider than 990px - eg: Desktop)", 'avia_framework'),
          "id"   => "av-desktop-hide",
          "std"   => "",
          "container_class" => 'av-multi-checkbox',
          "type"   => "checkbox"
        ),

        array(

          "desc"   => __("Hide on medium sized screens (between 768px and 989px - eg: Tablet Landscape)", 'avia_framework'),
          "id"   => "av-medium-hide",
          "std"   => "",
          "container_class" => 'av-multi-checkbox',
          "type"   => "checkbox"
        ),

        array(

          "desc"   => __("Hide on small screens (between 480px and 767px - eg: Tablet Portrait)", 'avia_framework'),
          "id"   => "av-small-hide",
          "std"   => "",
          "container_class" => 'av-multi-checkbox',
          "type"   => "checkbox"
        ),

        array(

          "desc"   => __("Hide on very small screens (smaller than 479px - eg: Smartphone Portrait)", 'avia_framework'),
          "id"   => "av-mini-hide",
          "std"   => "",
          "container_class" => 'av-multi-checkbox',
          "type"   => "checkbox"
        ),


        array(
          "type"   => "close_div",
          'nodescription' => true
        ),




        array(
          "type"   => "close_div",
          'nodescription' => true
        ),



        array(
          "type"   => "close_div",
          'nodescription' => true
        ),

      );
    }

    /**
     * Frontend Shortcode Handler
     *
     * @param array $atts array of attributes
     * @param string $content text within enclosing form of shortcode element
     * @param string $shortcodename the shortcode found, when == callback name
     * @return string $output returns the modified html string
     */
    function shortcode_handler($atts, $content = "", $shortcodename = "", $meta = "")
    {

      extract(AviaHelper::av_mobile_sizes($atts)); //return $av_font_classes, $av_title_font_classes and $av_display_classes 

      avia_sc_responsive_columns::$count++;
      $atts = shortcode_atts(array(
        'position'      => 'top left',
        'repeat'      => 'no-repeat',
        'attach'      => 'scroll',
        'background'    => '',
        'custom_bg'      => '',
        'attachment' => '',
        'attachment_size' => '',
        'color'      => 'main_color',
        'padding' => 'default',
        'border'    => '',
        'min_height'  => '0',
        'min_height_percent' => '',
        'mobile'    => 'av-erc-stack-cells',
        'mobile_breaking' => '',
        'id'      => '',
        'grid_width' => 'fullwidth',

      ), $atts, $this->config['shortcode']);

      extract($atts);
      $output    = "";
      $background = "";

      $fullwidth = $grid_width === 'fullwidth';
      $fullwidth_class = $fullwidth ? 'av-erc-fullwidth' : 'av-erc-content-width';

      $params['class'] = "av-layout-grid-container entry-content-wrapper av-flex-cells {$color} {$mobile} {$mobile_breaking} {$av_display_classes} {$border} {$fullwidth_class} avia-section-{$padding} " . $meta['el_class'];
      $params['open_structure'] = !$fullwidth;
      $params['id'] = !empty($id) ? AviaHelper::save_string($id, '-') : "av-layout-responsive-cols-" . avia_sc_responsive_columns::$count;
      $params['custom_markup'] = $meta['custom_markup'];



      if ($min_height_percent != "") $params['class'] .= " av-cell-min-height av-cell-min-height-" . $min_height_percent;

      $src = '';
      if (!empty($attachment) && !empty($attachment_size)) {
        /**
         * Allows e.g. WPML to reroute to translated image
         */
        $posts = get_posts(
          array(
            'include'      => $attachment,
            'post_status'    => 'inherit',
            'post_type'      => 'attachment',
            'post_mime_type'  => 'image',
            'order'        => 'ASC',
            'orderby'      => 'post__in'
          )
        );

        if (is_array($posts) && !empty($posts)) {
          $attachment_entry = $posts[0];

          $src = wp_get_attachment_image_src($attachment_entry->ID, $attachment_size);
          $src = !empty($src[0]) ? $src[0] : "";
        }
      } else {
        $attachment = false;
      }

      if ($custom_bg != "") {
        $background .= "background-color: {$custom_bg}; ";
      }

      /*set background image*/
      if ($src != "") {
        if ($repeat == 'stretch') {
          $background .= "background-repeat: no-repeat; ";
          $params['class'] .= " avia-full-stretch";
        } else if ($repeat == "contain") {
          $background .= "background-repeat: no-repeat; ";
          $params['class'] .= " avia-full-contain";
        } else {
          $background .= "background-repeat: {$repeat}; ";
        }

        $background .= "background-image: url({$src})";
        $background .= ";";
        $background .= $attach == 'parallax' ? "background-attachment: scroll; " : "background-attachment: {$attach}; ";
        $background .= "background-position: {$position}; ";



        if ($attach == 'parallax') {
          $attachment_class = "";
          if ($repeat == 'stretch' || $repeat == 'no-repeat') {
            $attachment_class .= " avia-full-stretch";
          }
          if ($repeat == 'contain') {
            $attachment_class .= " avia-full-contain";
          }

          $params['class'] .= " av-parallax-section";
          $speed = apply_filters('avf_parallax_speed', "0.3", $params['id']);
          $params['attach'] .= "<div class='av-parallax' data-avia-parallax-ratio='{$speed}' >";
          $params['attach'] .= "<div class='av-parallax-inner {$color} {$attachment_class}' style = '{$background}' >";
          $params['attach'] .= "</div>";
          $params['attach'] .= "</div>";
          $background = "";
        }


        $params['data'] = "data-section-bg-repeat='{$repeat}'";
      } 

      if ($custom_bg != "" && $background == "") {
        $background .= "background-color: {$custom_bg}; ";
      }


      //we dont need a closing structure if the element is the first one or if a previous fullwidth element was displayed before
      if (isset($meta['index']) && $meta['index'] == 0) $params['close'] = false;
      if (!empty($meta['siblings']['prev']['tag']) && in_array($meta['siblings']['prev']['tag'], AviaBuilder::$full_el_no_section)) $params['close'] = false;

      if (isset($meta['index']) && $meta['index'] != 0) $params['class'] .= " submenu-not-first";


      $params['class'] .= " avia-bg-style-" . $attach;
      $params['class'] = $params['class'] . " " . $meta['el_class'] . " " . $av_display_classes;
      $params['bg']    = $background;

      avia_sc_cell::$attr = $atts;
      $output .=  avia_new_section($params);
      $output .=  ShortcodeHelper::avia_remove_autop($content, true);

      if (!$fullwidth) {
        $output = str_replace('flex_cell', 'flex_column', $output);
        $cm     = avia_section_close_markup();
        $output .= "</div></div>{$cm}</div>";
      }

      $output .= avia_section_after_element_content($meta, 'after_responsive_col_' . avia_sc_responsive_columns::$count, false);

      return $output;
    }
  }
}
