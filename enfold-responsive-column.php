<?php
/**
 * Plugin Name: Enfold Responsive Column
 * Description: Adds a row with a 50/50 split that will re-order based on screen size
 * Version: 1.0.4
 * Author: Digital Operative
 * Author URI: https://digitaloperative.co.nz
 * BitBucket Plugin URI: https://bitbucket.org/digital-operative/enfold-responsive-column
 */

class Enfold_Responsive_Column
{
  const VERSION = '1.0.3';
  public static function init()
  {
    add_filter('avia_load_shortcodes', array(__class__, 'avia_load_shortcodes'), 15, 1);
    add_filter('avf_fwd_elements', array(__class__, 'avf_fwd_elements'));
  }

  /**
   * Returns the absolute path to the plugin
   */
  public static function get_dir(){
    return trailingslashit(plugin_dir_path(__FILE__)); 
  } 

  /**
   * Returns the URL to the plugin
   */
  public static function get_url() {
    return trailingslashit(plugins_url('', __FILE__));
  }

  /**
   * Adds in our shortcode directory
   */
  public static function avia_load_shortcodes($paths) {
    // add our path to the beginning so it gets checked first
    array_unshift($paths, self::get_dir() . 'templatebuilder/');
    return $paths;
  }

  /**
   * Adds this custom element as a full width element
   */
  public static function avf_fwd_elements($elements) {
    $elements[] = 'av_layout_responsive_columns';
    return $elements;
  }
}

Enfold_Responsive_Column::init();

